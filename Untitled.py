#!/usr/bin/env python
# coding: utf-8

# In[1]:


#pip install --upgrade "ibm-watson>=4.7.1"


# In[2]:


#pip install watson_developer_cloud


# In[3]:


#pip install pandas


# In[4]:


#pip install scikit-learn


# In[1]:


import json
from ibm_watson import AssistantV2
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from ibm_watson import ApiException
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
#importar métricas
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import mean_squared_error
from sklearn.metrics import accuracy_score
from joblib import dump, load


# In[2]:


iam_apikey = '6qq1YtIJOcVHfo8gUUlT_0cL6szs9qjPOtRpYURGYQT6'
v = '2020-09-13'
botURL = 'https://gateway.watsonplatform.net/assistant/api'
assistant_id = '18ea3248-1f1d-44ca-922f-444776456388'


# In[7]:


try:
    # autenticando asistente
    authenticator = IAMAuthenticator(iam_apikey)
    assistant = AssistantV2(version = v,authenticator = authenticator)
    assistant.set_service_url(botURL)
    # creando una sesion con nuestro dialogo productos
    response = assistant.create_session(assistant_id=assistant_id).get_result()
    # tranformar el json de la sesion para obtener el id
    val = json.loads(json.dumps(response, indent=2))
    # obtenemos id de la sesion, las sesiones se cierren automaticamente despues de un tiempo de inactividad
    session_id = val['session_id']
    # pregunta para el asistente
    pregunta = 'hola'
    # enviando mensaje de entrada al asistente
    # variable response almacena las respuestas del asistente
    response = assistant.message_stateless(assistant_id = assistant_id,
    input={
        'message_type': 'text',
        'text': pregunta
    }).get_result()
    # obtener el intento para identificar cuando quiere una recomendación
    intento = response['output']['intents'][0]['intent']
    # obtener el mensaje de respuesta por el asistente
    respuesta = response['output']['generic'][0]['text']
    print(intento,' - ', respuesta)
    # condicion para cuando el usuario desea una recomendación
    # y solo realize recomendaciones cuando se le pida 
    if(intento=='recomendar'):
        print('peliculas recomendadas')
    
except ApiException as ex:
    print ("Method failed with status code " + str(ex.code) + ": " + ex.message)


# In[3]:


# leer archivo csv
# una persona compra productos en determinada fechas
# por lo cual se repite el usuario varias veces dependiendo
# los días de compra que posee
data2 = pd.read_csv('customers_orders_3.csv', delimiter=';')


# In[4]:


# dataframe vacio
productos = pd.DataFrame()
data3 = pd.DataFrame()
# sumar cuantas veces compro una persona dicho producto y resetear los index a columnas
data3 = data2.groupby(['CUSTNAME','GenderCode','AGE','PURCHASE_STATUS']).sum().reset_index()


# In[5]:


# reemplazar a valores numericos para el modelo
# mujer-0, hombre-1
data3.GenderCode.replace(['Mr.','Master.','Mrs.','Miss.'], [1,1,0,0], inplace=True)
# convertir la frecuencia a numeros
data3.PURCHASE_STATUS.replace(['FirstTime', 'Occasional', 'Frequent'], [1,2,3], inplace=True)


# In[6]:


# con estos datos vamos a entrenar el modelo
# usuarios
user = data3.loc[:,'CUSTNAME']
# edad de los usuarios
age = data3.loc[:,'AGE']
# genero de los usuarios
genero = data3.loc[:,'GenderCode']
# productos de la tienda
prod = data3.loc[:,'Baby Food':'Spices']
# frecuencia de cada usuario
estado = data3.loc[:,'PURCHASE_STATUS']


# In[7]:


# construyendo matriz de compras para la predicción
matriz = pd.concat([user,age,genero,estado, prod], axis=1)
#matriz.to_csv('hola.csv', index=False, header=True)


# In[8]:


# lista de productos mas vendidos
#list(dict(pd.DataFrame(matriz.loc[:,'Baby Food':'Spices'].sum()).sort_values(by=0, ascending=False)[0]).keys())


# In[9]:


def soloCompra(lista_productos,data):
    productos=[]
    v =[]
    # ciclo for para obtener todos los votos de todos los usuario y compra
    for i in range(0,len(lista_productos)):
        v=list(data[lista_productos[i]])
        for x in range(len(v)):
            productos.append(v[x])
    return productos
def nombre_user(lista_usuarios,lista_genero,lista_edad,lista_estado,votos):
    id_usuario = []
    nombre_usuario = []
    genero_usuario = []
    edad_usuario = []
    estado_usuario = []
    limite = 0
    # operacion para obtener un limite para la creacion de los compra y tener una referencia
    # para luego ser repetidos y asignarles a cada voto
    limite = len(votos)/len(lista_usuarios)
    # ciclo para prepara la asignacion de compra y idhashtag a cada voto
    for ii in range(0,len(lista_usuarios)):
        for iii in range(0,int(limite)):
            id_usuario.append(ii)
            nombre_usuario.append(lista_usuarios[ii])
            genero_usuario.append(lista_genero[ii])
            edad_usuario.append(lista_edad[ii])
            estado_usuario.append(lista_estado[ii])
    return id_usuario, nombre_usuario, genero_usuario, edad_usuario, estado_usuario
def producto(lista_producto, compra):
    id_compra=[]
    nombre_compra = []
    limite2 = 0
    # operacion para obtener un limite para la creacion de los usuarios y tener una referencia
    # para luego ser repetidos y asignarles a cada voto
    limite2 = len(compra)/len(lista_producto)
    aux2 = 0
    # ciclo para prepara la asignacion de usuarios y idusuarios a cada voto con su respectivo compra
    for j in range(0,int(limite2)):
        for jj in range(0,len(lista_producto)):
            nombre_compra.append(lista_producto[jj])
            id_compra.append(jj)
    return id_compra, nombre_compra


# In[10]:


# lista de usuarios
lista_usuarios = list(matriz['CUSTNAME'])
# lista sexo usuarios
lista_genero = list(matriz['GenderCode'])
# lista edad usuarios
lista_edad = list(matriz['AGE'])
# lista estado usuarios
lista_estado = list(matriz['PURCHASE_STATUS'])
# lista de productos
lista_producto = list(matriz.columns)[4:-2]
# obtener la lista de compras de los usuarios en una lista
compras = soloCompra(lista_producto,matriz)
# metodo que retorna la lista de id de usuarios, genero, edad, estado junto con sus nombres
id_usuario, nombre_usuario, genero_usuario, edad_usuario, estado_usuario = nombre_user(lista_usuarios, lista_genero, lista_edad
                                                          ,lista_estado , compras)
# lista de compras realizadas por los usuarios y los nombres de los productos
id_compra, nombre_producto = producto(lista_producto, compras)


# In[11]:


# creamos un diccionario donde cada usuario se le asigna su producto y si fue comprado
dato2={'userName':nombre_usuario,'userId':id_usuario,'productoNombre':nombre_producto,
       'productoId':id_compra,'compra':compras,'generoId':genero_usuario, 'edad':edad_usuario,
      'estadoId':estado_usuario}
# creamos un dataframe para controlar y visualizar de moejor manera los datos 
df2 = pd.DataFrame(dato2)


# In[12]:


# al estar seguro de las asignaciones de los productos y compras, solo seleccionamos los campos necesarios
# en este caso usamos el modelo knn donde todos los valores deben ser numericos
dato3 ={'userId':id_usuario,'productoId':id_compra,'generoId':genero_usuario,'edad':edad_usuario,
        'estadoId':estado_usuario,'compra':compras}
# todos los datos del diccionatio convertimos en dataframe para mejor visualizacion
df22 = pd.DataFrame(dato3)
# seleccionamos valores que son utilizados para el analisis del modelo
X = df22[['productoId','generoId','edad','estadoId']].values
# son los valores de predicción para el modelo
y = df22['compra'].values


# In[13]:


# para cualquier modelo de IA se debe dividir los datos en prueba y entrenamiento
# entrenamiento -> dichos datos sirven para entrenar el modelo 
# prueba -> sirven para la utilizacion de ejecutar las metricas y conocer 
# la precisión de los modelos en sus predicciones
# los datos de prueba y entrenamienyo se las consigue dividiendo la data general en %80 para entrenar 
# y %20 para pruebas, esto permite que el modelo se entrene con datos conocidos
# mientras que los de prueba predice con datos que desconoce y calcula cuantos son fueron correctamente 
# predecidos
# variable para el porcentaje para dividir los datos
split_data = 0.2
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size= split_data)


# In[40]:


# ciclo para fundamentar el numero de vecinos adecuado para el modelo
# para esto es debe elegir los valores mas ceras a 1 pero no debe pasar de 1 o debe llegar a
# 1 ya que no existe modelos que recomienden al 100% si esto existe entonces tendriamos
# que hablar sobre problemas de los datos para entrenar los modelos
for i in range(1,30):
    # parametros del modelo como numero de vecinos mas cercanos
    neigh = KNeighborsClassifier(n_neighbors=i)
    print(neigh)
    # entrenado el modelo con datos de entrenamiento
    neigh.fit(X_train, y_train)
    # realizando predicciones con datos desconocidos por el modelo
    prediccion_y = neigh.predict(X_test)
    # el modelo predice que productos puede recomendar cuando es 1 se le recomendara al usuario
    # y cuando es 0 no es para el usuario
    #print(prediccion_y)
    # METRICAS PARA EVALUAR MODELO
    accuracy = accuracy_score(y_test, prediccion_y)
    precision = precision_score(y_test, prediccion_y, average='micro')
    recall = recall_score(y_test, prediccion_y, average='micro')
    # “Accuracy” se refiere a lo cerca que está el resultado 
    # “Precision” Cuanto menor es la dispersión mayor la precisión
    # “Recall” proporción de casos positivos que fueron correctamente identificadas por el algoritmo
    print(accuracy,' - ',precision,' - ',recall)


# In[54]:


# parametros del modelo como numero de vecinos mas cercanos
neigh = KNeighborsClassifier(n_neighbors=6)
# entrenado el modelo con datos de entrenamiento
neigh.fit(X_train, y_train)


# In[55]:


# persistencia del modelo entenado.- es para evitar que el modelo se entrene cada rato
# para realizar las recomendaciones de forma mas rapida
dump(neigh, 'recom.joblib')


# In[65]:


# cargamos modelo pre entrenado
clf = load('recom.joblib')
# para conocer si un usuario compraria un producto se debe ingresar los id de los
# parametros que requiere el modelo para realizar la predicción
# [idProducto, idgenero, edad, idEstado]
# para conocer los id de los productos estos se encuentran en la varible lista_producto
# idGenero -> 0-mujer, 1-hombre
# idEstado -> primeravez, ocasional, frecuente


# In[66]:


# ciclo par recorrer todos los productos
idGenero = 0
edad = 25
idEstado = 1
for idProducto in range(0,len(lista_producto)):
    pred = clf.predict([[idProducto,idGenero,edad,idEstado]])
    #print(idProducto,'-',idGenero,'-',edad,'-',idEstado)
    # condicion para que imprima las recomendaciones basado en datos
    # del usuario como genero, edad, estado
    if pred[0]>0:
        print(lista_producto[idProducto])


# In[106]:


# ahora el usuario ya ingreso los productos que compro o le gusto
# para esto en base a lo que el usuario eligio, en el chat
# se va visualizar las recomendaciones dependiendo del producto elegido
from scipy.sparse import csr_matrix
# convertir las compras en funciones de productos
df_prod_features = df2.pivot(index='productoNombre',columns='userId',values='compra').fillna(0)
# convertir el marco de datos de las características del producto en una matriz dispersa scipy
mat_prod_features = csr_matrix(df_prod_features.values)


# In[107]:


from sklearn.neighbors import NearestNeighbors
# los vecinos mas cercanos
model_knn = NearestNeighbors(metric='cosine', algorithm='brute')
# entrenamos modelo con todos los datos
model_knn.fit(mat_prod_features)


# In[108]:


import numpy as np
# ingresamos id del producto elegido por el usuario
query_index = 12
# numero de productos que se desea visualizar en el chat
recom = 6
# econtramos los productos y la distancia que tiene en relación
# del producto escogido
distances, indices = model_knn.kneighbors(df_prod_features.iloc[query_index,:].values.reshape(1, -1), n_neighbors = recom)
# ciclo para recorrer las recomendaciones 
for i in range(0, len(distances.flatten())):
    if i == 0:
        print('Recomendaciones para el {0}:\n'.format(df_prod_features.index[query_index]))
    else:
        print('{0}: {1}'.format(i, df_prod_features.index[indices.flatten()[i]]))


# In[ ]:




