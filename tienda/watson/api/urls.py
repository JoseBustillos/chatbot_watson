from django.urls import path, include

from watson.api.chatbot.api import ChatsmsView


urlpatterns=[
    path("chatbot/",ChatsmsView.as_view()),
]