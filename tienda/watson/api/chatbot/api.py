import random
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import serializers, status
from watson.models import Producto
from watson.modulo.knn.chat.recomendar import productos
from watson.modulo.chat.asistente import asistente
from watson.models import UsuarioProducto
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from watson.api.chatbot.serializer import ChatSerializer



class ChatsmsView(APIView):
    intento=''
    nuevo = []

    def post(self, request, format=None):
        nuevo=[]
        respuest_chat=""
        # llamamos la clase de serializer
        serializer = ChatSerializer(data=request.data)
        if serializer.is_valid():
            # obtenemos respuesta de watson que es intento y mensaje
            intento, respuesta, respuesta2 = asistente(serializer.data["sms"].get("sms"))
            user_pk =serializer.data["sms"].get("user")
            respuest_chat=""
            respuest_chat=respuesta
            if intento=='RecomendacionProducto':
                if not respuesta2 is None:
                    producto = Producto.objects.filter(producto__icontains=respuesta2)
                    if  producto.exists():
                        respuesta="Si tenemos dicho producto: "+str(producto[0])+". También te pueden interesar otros productos como:"
                        recomendaciones =", ".join(productos(producto[0].pk))
                        respuest_chat=respuesta+" "+recomendaciones
                else:
                    respuesta="Te puede interesar dichos productos como:"
                    idProduct = list(UsuarioProducto.objects.filter(usuario__pk=user_pk).values('producto__id'))
                    for i in range(0,len(idProduct)):
                        nuevo.append(idProduct[i]['producto__id'])
                    recomendaciones =", ".join(productos(random.choice(nuevo)))
                    respuest_chat=respuesta+" "+recomendaciones
            data=respuest_chat
        return Response(data, status=status.HTTP_201_CREATED)
