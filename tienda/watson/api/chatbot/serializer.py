from rest_framework import serializers


class ChatSerializer(serializers.Serializer):
    """ ejemplo del JSON para API
    {"sms":"hola"}
    """
    sms = serializers.JSONField()
    print(sms)