


<div class="page-content page-container" id="page-content">
<div class="padding">
    <div class="row container d-flex justify-content-center">
        <div class="col-md-6">
            <div class="card card-bordered">
                <div class="card-header">
                    <h4 class="card-title"><strong>Chat</strong></h4>
                </div>
                <div class="ps-container ps-theme-default ps-active-y" id="chat-content"
                     style="overflow-y: scroll !important; height:400px !important;">
                    <!-- mensaje de entrada -->
                    <div class="media media-chat media-chat-reverse">
                        <div class="media-body">
                            <p>{{ sms }}</p>
                            <p class="meta">
                                <time datetime="2018">00:10</time>
                            </p>
                        </div>
                    </div>
                    <!-- mensaje asistente -->
                    <div class="media media-chat"><img class="avatar"
                                                       src="https://img.icons8.com/color/36/000000/administrator-male.png"
                                                       alt="...">
                        <div class="media-body">
                            <p>{{ resp }}</p>
                            {% if recomen|length > 0 %}
                                {% for i in recomen %}
                                    <p>{{i}}</p>
                                {%endfor%}
                            {% endif %}
                        </div>
                    </div>
                </div>
                <div class="publisher bt-1 border-light">
                    <img class="avatar avatar-xs"
                         src="https://img.icons8.com/color/36/000000/administrator-male.png"
                         alt="...">

                    <form method="POST" enctype="multipart/form-data"
                          class="form-horizontal">
                        {% csrf_token %}
                        <div class="container">
                            <div class="row">
                                <div class="col">{{ form }}</div>
                                <button class="btn"><i class="far fa-paper-plane"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
