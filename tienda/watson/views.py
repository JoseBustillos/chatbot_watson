import random
import pandas as pd
import datetime
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as dj_login
from django.contrib import messages
from django.contrib.auth.models import User
from .forms import ChattForm, UsuarioForm, EntrenarForm, ProfileForm, ProductoForm
from joblib import load
from .models import Sexo, Frecuencia, Producto, PerfilUsuario, UsuarioProducto
from django.contrib.auth.models import User
from .modulo.chat.asistente import asistente
from .modulo.knn.preProcesamiento.matrizCompra import cambioData
from .modulo.knn.entrenamiento.entrenar import entrenarM
from .modulo.knn.preProcesamiento.dataLista import matrizCom, producto


def index(request):
    sms=''
    intento=''
    respuesta=''
    nuevo = []
    pr = []
    if request.user.is_authenticated:
        # cuando el usuario este logeado obtendra su id de bd
        idUser = request.user.id
        idProduct = list(UsuarioProducto.objects.filter(usuario=idUser).values('producto__id'))
        for i in range(0,len(idProduct)):
            nuevo.append(idProduct[i]['producto__id'])
    if request.method == "POST":
        try:
            form = ChattForm(request.POST)
            if form.is_valid():
                sms = form.cleaned_data['mensaje']
                intento, respuesta = asistente(sms)
                # condicion recomendador
                if intento=='recomendar':
                    pr = "hola"
                    product=" ".join(producto(random.choice(nuevo)))
        except Exception as e:
            print(e)
    else:
        form = ChattForm()
    return render(request, 'watson/index2.html',{'form':form, 'sms':sms,'intent': intento, 'resp':respuesta
                  ,'recomen':pr})

def new(request):
    if request.method=='POST':
        form = UsuarioForm(request.POST)
        form1 = ProfileForm(request.POST)
        form2 = ProductoForm(request.POST)
        if form.is_valid() and form1.is_valid() and form2.is_valid():
            nombre = form.cleaned_data.get('nombres')
            apellido = form.cleaned_data.get('apellidos')
            contrasena = form.cleaned_data.get('password1')
            usuario = form.cleaned_data.get('usuario')
            email = form.cleaned_data.get('email')
            fecha_nacimiento= form1.cleaned_data.get('fecha_nacimiento')
            sexo = form1.cleaned_data.get('sexo')
            # retorna solo los productos que seleccionen
            productos = form2.cleaned_data.get('producto')
            ltId = list(productos.values('id'))
            user = User(username=usuario, first_name=nombre, last_name=apellido, email=email,
                                            date_joined=datetime.datetime.now(), is_superuser=False,
                                            is_staff=True, is_active=True)
            user.set_password(contrasena)
            user.save()
            sex = Sexo.objects.get(id=sexo)
            frec = Frecuencia.objects.get(id=1)
            aaa = User.objects.get(username=nombre, first_name=nombre, last_name=apellido, email=email)
            p = PerfilUsuario(fecha_nacimiento=fecha_nacimiento, frecuencia=frec, sexo=sex,usuario=aaa)
            p.save()
            # ciclo para asignar usuario producto
            for i in range(0,len(ltId)):
                # consultar productos
                produ = Producto.objects.get(id=ltId[i]['id'])
                # asignar usuario producto
                pro = UsuarioProducto(usuario=aaa, producto=produ)
                # almacenar en bd
                pro.save()
            # redireccionar a la pagina login
            return redirect('login')
    else:
        form = UsuarioForm()
        form1 = ProfileForm()
        form2 = ProductoForm()
    return render(request, 'watson/new.html', {'form': form,'form1': form1,'form2': form2})

def login(request):
    if request.method=='POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            dj_login(request, user)
            return redirect('index')
        else:
            messages.info(request,'Usuario o contraseña incorrecta')
    return render(request, 'watson/login.html')

def entrenar(request):
    # lista para guardar los datos de cursos y estudiante
    accuracy = 0
    recall = 0
    precision = 0
    datos=[]
    if request.method == "POST":
        try:
            csv_file = request.FILES["csv_file"]
            # verificar si es un archivo csv
            data_set = csv_file.read().decode('UTF-8')
            # obtener lineas del csv
            data = (data_set.split('\n'))
            # obtener nombre de las columnas
            colums = data[0].split(';')
            co = [i.replace('\r', '') for i in colums]
            # de las lineas convertir en una lista
            for i in range(1,len(data)):
                tam = len(data[i].split(';'))
                # verificar tamaño de la lista
                if tam == len(colums):
                    # todas las listas de datos se agrega nueva lista
                    dt = data[i].split(';')
                    datos.append([i.replace('\r', '') for i in dt])
            # creando dataframe
            df = pd.DataFrame(datos,columns=co)
            form = EntrenarForm(request.POST)
            if form.is_valid():
                vecino = form.cleaned_data.get('vecino')
                # generando matriz de compra
                m = cambioData(df)
                # data preparada
                dat = matrizCom(m)
                accuracy, precision, recall = entrenarM(dat,vecino)
        except Exception as e:
            print(e)
    else:
         form=EntrenarForm()

    return render(request, 'watson/entrenar.html',{'form':form,'accuracy': accuracy, 'recall': recall,
                   'precision': precision})

def cargarProduct(request):
    idPro=[]
    # obtnemos el sexo y el año de nacimiento por el ajax
    id_sex = request.GET.get("sexo")
    anio = request.GET.get("edad")
    # control de varibles
    if anio is None:
        edad = 18
    else:
        # calcular edad del usuario
        edad = 2020 - int(anio)
    # asingnar id del sexo usuario
    if id_sex is None:
        id_sexo = 1
    else:
        id_sexo = int(id_sex)
    # cargando modelo preentrenado
    clf = load('recom.joblib')
    # lista de productos
    pro = ['Comida para bebés', 'Pañales', 'Fórmula', 'Loción', 'Jabón para bebés', 'Toallitas húmedas', 'Frutas frescas',
                'Verduras frescas', 'Cerveza', 'Vino', 'Club Soda', 'Bebida deportiva',
                'Patatas fritas', 'Palomitas de maíz', 'Harina de avena', 'Medicamentos', 'Alimentos enlatados', 'Cigarrillos', 'Queso',
                'Productos de limpieza', 'Condimentos', 'Alimentos congelados', 'Artículos de cocina', 'Carne',
                'Suministros de oficina', 'Cuidado personal', 'Suministros para mascotas']
    # ciclo para recorrer los productos
    for idProducto in range(0, len(pro)):
        # predecir si el producto es ideal para el usuario
        pred = clf.predict([[idProducto, id_sexo, edad, 1]])
            # print(idProducto,'-',idGenero,'-',edad,'-',idEstado)
            # condicion para que imprima las recomendaciones basado en datos
            # del usuario como genero, edad, estado
        # almacena solo los productos que son para el usuario en base al modelo buscando en la base de datos
        # para obtener el id de registro
        if pred[0] > 0:
            idPro.append(list(Producto.objects.filter(
                    producto__icontains=pro[idProducto]).values('id'))[0]['id'])
    # el modelo no predice 10 producto por lo cual
    # generamos una lista de id de productos aleatorios para que se presente los productos faltantes
    # y completar los productos
    aux = random.sample(range(2,20),10-len(idPro))
    # si el modelo predice algunos productos actualiza su lista con la lista de aleatorios
    idPro.extend(aux)
    # realizamos una consulta a la base de datos con la lista de id de productos generado
    valor = Producto.objects.filter(id__in=idPro)
    return render(request, 'watson/form_list_product.html', {'productos': valor})
