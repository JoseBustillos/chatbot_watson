from django.db import models
from django.contrib.auth.models import User



class Frecuencia(models.Model):
    frecuencia = models.CharField(max_length=200)


class PerfilUsuario(models.Model):
    fecha_nacimiento = models.DateTimeField()
    frecuencia = models.ForeignKey(Frecuencia, on_delete=models.PROTECT, null=True)
    sexo = models.ForeignKey('Sexo', on_delete=models.PROTECT, null=True)
    usuario = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    anot = models.IntegerField(null=True)


class Producto(models.Model):
    producto = models.CharField(max_length=200)
    img_producto = models.ImageField(upload_to="producto", max_length=200, null=True, blank=True)

    def __str__(self) -> str:
        return self.producto


class Sexo(models.Model):
    sexo = models.CharField(max_length=20)


class UsuarioProducto(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    producto = models.ForeignKey(Producto, on_delete=models.PROTECT, null=True)

