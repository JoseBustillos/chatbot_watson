from django import forms
from .models import Producto, Sexo
from django.utils.safestring import mark_safe
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class CustomChoiceField(forms.ModelChoiceField):

    def label_from_instance(self, obj):
        print(obj)
        return mark_safe("<img src="'%s' % obj.img_producto.url)


class ChattForm(forms.Form):
    mensaje = forms.CharField(required=False)


BIRTH_YEAR_CHOICES = range(1980, 2020)


class UsuarioForm(UserCreationForm):
    usuario = forms.CharField(max_length=30, required=True)
    nombres = forms.CharField(max_length=30, required=True)
    apellidos = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('usuario', 'nombres', 'apellidos', 'email', 'password1',
                  'password2')

class ProfileForm(forms.Form):
    sexo = forms.ChoiceField(choices=[(1, 'Masculino'), (2, 'Femenino')])
    fecha_nacimiento = forms.DateField(widget=forms.SelectDateWidget(years=BIRTH_YEAR_CHOICES))

class ProductoForm(forms.Form):
    producto = forms.ModelMultipleChoiceField(queryset=Producto.objects.all(),
                                              required=True,
                                              widget=forms.CheckboxSelectMultiple())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['producto'].queryset = Producto.objects.all()
        if 'producto' in self.data:
            try:
                idPro = [int(i) for i in dict(self.data)['producto']]
                self.fields['producto'].queryset = Producto.objects.filter(id__in=idPro)
            except (ValueError, TypeError):
                pass


class EntrenarForm(forms.Form):
    vecino = forms.IntegerField()
