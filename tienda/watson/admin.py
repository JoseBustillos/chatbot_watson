from django.contrib import admin
from .models import Producto, Frecuencia, Sexo, UsuarioProducto, PerfilUsuario


# Register your models here.
class AuthUserAdmin(admin.ModelAdmin):
    pass


class ProductoAdmin(admin.ModelAdmin):
    list_display = ('producto', 'img_producto')


class FrecuenciaAdmin(admin.ModelAdmin):
    list_display = ('frecuencia',)


class SexoAdmin(admin.ModelAdmin):
    list_display = ("sexo",)


class UsuarioProductoAdmin(admin.ModelAdmin):
    list_display = ("usuario", "producto")


class PerfilUsuarioAdmin(admin.ModelAdmin):
    list_display = ("fecha_nacimiento", "frecuencia", "sexo", "usuario")


admin.site.register(Producto, ProductoAdmin)
admin.site.register(Frecuencia, FrecuenciaAdmin)
admin.site.register(Sexo, SexoAdmin)
admin.site.register(UsuarioProducto, UsuarioProductoAdmin)
admin.site.register(PerfilUsuario, PerfilUsuarioAdmin)
