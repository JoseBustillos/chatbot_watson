from pickle import NONE
from ibm_watson import AssistantV2
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from ibm_watson import ApiException
from requests.models import Response
from .credencial import credenciales
import json
def asistente(pregunta):
    label_suggest=""
    respuesta2=None
    respuesta=None
    intento=""
    iam_apikey, v, botURL, assistant_id = credenciales()
    # autenticando asistente
    authenticator = IAMAuthenticator(iam_apikey)
    assistant = AssistantV2(version=v, authenticator=authenticator)
    assistant.set_service_url(botURL)
    # creando una sesion con nuestro dialogo productos
    assistant.create_session(assistant_id=assistant_id).get_result()
    # pregunta para el asistente
    # enviando mensaje de entrada al asistente
    # variable response almacena las respuestas del asistente
    response = assistant.message_stateless(assistant_id=assistant_id,
                                               input={
                                                   'message_type': 'text',
                                                   'text': pregunta
                                               }).get_result()
    # obtener el intento para identificar cuando quiere una recomendación
    response = response['output']
    print(response)
    if "entities" in response:
        if len(response["intents"]) == 0:
            intento = "Ninguno"
        else:
            intento = response['intents'][0]['intent']
        if len(response['entities']) > 0:
            respuesta2 = response['entities'][0]['value']
    if "generic" in response:
        if len(response['intents']) > 0:
            intento = response['intents'][0]['intent']
        else:
            intento = "Ninguno"
        if "text" in response['generic']:
            respuesta = response['generic'][0]['text']
        if "text" in response['generic'][0]:
            respuesta = response['generic'][0]['text']

        elif "suggestions" in response['generic']:
            respuesta = response['generic'][0]['title']
            for i in response['generic'][0]['suggestions']:
                respuesta += " "+(i["label"])


    print(intento, " ",respuesta," ",respuesta2)
    return intento, respuesta,respuesta2