import pandas as pd
def cambioData(data2):
    # reemplazar a valores numericos para el modelo
    # mujer-0, hombre-1
    data2.GenderCode.replace(['Mr.', 'Master.', 'Mrs.', 'Miss.'], [1, 1, 0, 0], inplace=True)
    # convertir la frecuencia a numeros
    data2.PURCHASE_STATUS.replace(['FirstTime', 'Occasional', 'Frequent'], [1, 2, 3], inplace=True)
    # con estos datos vamos a entrenar el modelo
    # usuarios
    user = data2.loc[:, 'CUSTNAME']
    # edad de los usuarios
    age = data2.loc[:, 'AGE']
    # genero de los usuarios
    genero = data2.loc[:, 'GenderCode']
    # productos de la tienda
    prod = data2.loc[:, 'Baby Food':'Spices'].astype(int)
    # frecuencia de cada usuario
    estado = data2.loc[:, 'PURCHASE_STATUS']
    # construyendo matriz de compras para la predicción
    matriz = pd.concat([user, age, genero, estado, prod], axis=1)
    # sumar cuantas veces compro una persona dicho producto y resetear los index a columnas
    dt = matriz.groupby(['CUSTNAME', 'GenderCode', 'AGE', 'PURCHASE_STATUS']).sum().reset_index()
    return dt