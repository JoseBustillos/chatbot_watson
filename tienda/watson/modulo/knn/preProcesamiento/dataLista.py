import pandas as pd

def soloCompra(lista_productos,data):
    productos=[]
    # ciclo for para obtener todos los votos de todos los usuario y compra
    for i in range(0,len(lista_productos)):
        v=list(data[lista_productos[i]])
        for x in range(len(v)):
            productos.append(v[x])
    return productos
    
def nombre_user(lista_usuarios,lista_genero,lista_edad,lista_estado,votos):
    id_usuario = []
    nombre_usuario = []
    genero_usuario = []
    edad_usuario = []
    estado_usuario = []
    # operacion para obtener un limite para la creacion de los compra y tener una referencia
    # para luego ser repetidos y asignarles a cada voto
    limite = len(votos)/len(lista_usuarios)
    # ciclo para prepara la asignacion de compra y idhashtag a cada voto
    for ii in range(0,len(lista_usuarios)):
        for iii in range(0,int(limite)):
            id_usuario.append(ii)
            nombre_usuario.append(lista_usuarios[ii])
            genero_usuario.append(lista_genero[ii])
            edad_usuario.append(lista_edad[ii])
            estado_usuario.append(lista_estado[ii])
    return id_usuario, nombre_usuario, genero_usuario, edad_usuario, estado_usuario

def producto(lista_producto, compra):
    id_compra=[]
    nombre_compra = []
    # operacion para obtener un limite para la creacion de los usuarios y tener una referencia
    # para luego ser repetidos y asignarles a cada voto
    limite2 = len(compra)/len(lista_producto)
    # ciclo para prepara la asignacion de usuarios y idusuarios a cada voto con su respectivo compra
    for j in range(0,int(limite2)):
        for jj in range(0,len(lista_producto)):
            nombre_compra.append(lista_producto[jj])
            id_compra.append(jj)
    return id_compra, nombre_compra

def matrizCom(matriz):
    # lista de usuarios
    lista_usuarios = list(matriz['CUSTNAME'])
    # lista sexo usuarios
    lista_genero = list(matriz['GenderCode'])
    # lista edad usuarios
    lista_edad = list(matriz['AGE'])
    # lista estado usuarios
    lista_estado = list(matriz['PURCHASE_STATUS'])
    # lista de productos
    lista_producto = list(matriz.columns)[4:-2]
    # obtener la lista de compras de los usuarios en una lista
    compras = soloCompra(lista_producto, matriz)
    # metodo que retorna la lista de id de usuarios, genero, edad, estado junto con sus nombres
    id_usuario, nombre_usuario, genero_usuario, edad_usuario, estado_usuario = nombre_user(lista_usuarios, lista_genero,
                                                                                           lista_edad
                                                                                           , lista_estado, compras)
    # lista de compras realizadas por los usuarios y los nombres de los productos
    id_compra, nombre_producto = producto(lista_producto, compras)
    # creamos un diccionario donde cada usuario se le asigna su producto y si fue comprado
    dato2 = {'userName': nombre_usuario, 'userId': id_usuario, 'productoNombre': nombre_producto,
             'productoId': id_compra, 'compra': compras, 'generoId': genero_usuario, 'edad': edad_usuario,
             'estadoId': estado_usuario}
    # creamos un dataframe para controlar y visualizar de moejor manera los datos
    df2 = pd.DataFrame(dato2)
    df2.to_csv('dataRecomendar.csv')
    # al estar seguro de las asignaciones de los productos y compras, solo seleccionamos los campos necesarios
    # en este caso usamos el modelo knn donde todos los valores deben ser numericos
    dato3 = {'userId': id_usuario, 'productoId': id_compra, 'generoId': genero_usuario, 'edad': edad_usuario,
             'estadoId': estado_usuario, 'compra': compras}
    # todos los datos del diccionatio convertimos en dataframe para mejor visualizacion
    df22 = pd.DataFrame(dato3)
    return df22