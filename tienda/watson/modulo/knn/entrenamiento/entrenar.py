from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from joblib import dump
from .metricas import metricas
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

def entrenarM (df22,vecino):
    X = df22[['productoId', 'generoId', 'edad', 'estadoId']].values
    # son los valores de predicción para el modelo
    y = df22['compra'].values
    # variable para el porcentaje para dividir los datos
    split_data = 0.2
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=split_data)
    neigh = KNeighborsClassifier(n_neighbors=vecino)
    # entrenado el modelo con datos de entrenamiento
    neigh.fit(X_train, y_train)
    # realizando predicciones con datos desconocidos por el modelo
    prediccion_y = neigh.predict(X_test)
    accuracy, precision, recall = metricas(y_test, prediccion_y)
    print (accuracy,precision,recall)
    print(confusion_matrix(y_test, prediccion_y))
    print(classification_report(y_test, prediccion_y))
    dump(neigh, 'recom.joblib')
    return accuracy,precision,recall