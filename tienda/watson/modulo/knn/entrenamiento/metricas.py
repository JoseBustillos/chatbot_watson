#importar métricas
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import accuracy_score

def metricas(y_test,prediccion_y):
    accuracy = accuracy_score(y_test, prediccion_y)
    precision = precision_score(y_test, prediccion_y, average='micro')
    recall = recall_score(y_test, prediccion_y, average='micro')
    return accuracy,precision,recall