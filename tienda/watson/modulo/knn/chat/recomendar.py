import random
import numpy as np
from sklearn.neighbors import NearestNeighbors
from scipy.sparse import csr_matrix
import pandas as pd

def productos(idProducto):
    df2 = pd.read_csv('dataRecomendar.csv')
    # ahora el usuario ya ingreso los productos que compro o le gusto
    # para esto en base a lo que el usuario eligio, en el chat
    # se va visualizar las recomendaciones dependiendo del producto elegido
    # convertir las compras en funciones de productos
    df_prod_features = df2.pivot(index='productoNombre', columns='userId', values='compra').fillna(0)
    # convertir el marco de datos de las características del producto en una matriz dispersa scipy
    mat_prod_features = csr_matrix(df_prod_features.values)
    # los vecinos mas cercanos
    model_knn = NearestNeighbors(metric='cosine', algorithm='brute')
    # entrenamos modelo con todos los datos
    model_knn.fit(mat_prod_features)
    # ingresamos id del producto elegido por el usuario
    query_index = idProducto
    # numero de productos que se desea visualizar en el chat
    recom = 10
    # econtramos los productos y la distancia que tiene en relación
    # del producto escogido
    distances, indices = model_knn.kneighbors(df_prod_features.iloc[query_index, :].values.reshape(1, -1),
                                              n_neighbors=recom)
    # ciclo para recorrer las recomendaciones
    recomendado = []
    for i in range(0, len(distances.flatten())):
        recomendado.append(df_prod_features.index[indices.flatten()[i]])
    random.shuffle(recomendado)
    return recomendado
