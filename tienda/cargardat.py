from watson.models import Producto, Sexo, Frecuencia

list_product = ['Comida para bebés', 'Pañales', 'Fórmula', 'Loción', 'Jabón para bebés', 'Toallitas húmedas', 'Frutas frescas',
                'Verduras frescas', 'Cerveza', 'Vino', 'Club Soda', 'Bebida deportiva',
                'Patatas fritas', 'Palomitas de maíz', 'Harina de avena', 'Medicamentos', 'Alimentos enlatados', 'Cigarrillos', 'Queso',
                'Productos de limpieza', 'Condimentos', 'Alimentos congelados', 'Artículos de cocina', 'Carne',
                'Suministros de oficina', 'Cuidado personal', 'Suministros para mascotas']

for i in list_product:
    Producto.objects.get_or_create(producto=i)


list_sex = ['Masculino', 'Femenino', 'otro']

for i in list_sex:
    Sexo.objects.get_or_create(sexo=i)



list_frecuencia = ['Primera vez', 'Ocasional', 'Frecuente']

for i in list_frecuencia:
    Frecuencia.objects.get_or_create(frecuencia=i)